//
//  main.swift
//  MainJeux
//
//  Created by etudiant on 09/02/2023.
//

import Foundation

import HelloLibrary

let lecteur = Lecteur()
let afficheur = Afficheur()
var b = Board()

if let board = b {
    let player1 = Human(nom: "Arafa", playedOn: board, readOn: lecteur)
    let player2 = IA(nom: "Najlae", playedOn: board)
    let rule = ClassicRules(withBoard: board)
  var game = Game(withBoard: board, playedBy: [player1, player2], withRules: rule!, writeOn: afficheur)
  var winner : Player?
    while winner == nil && rule!.isGameOver()
    {
        winner = game?.tour()
    }
    
    afficheur.afficherLigne(message: "Le gagnant est \(winner?.nom)")
}


