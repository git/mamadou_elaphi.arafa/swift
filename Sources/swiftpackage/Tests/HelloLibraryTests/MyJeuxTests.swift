import XCTest
@testable import MyJeux

final class MyJeuxTests: XCTestCase {
    
    func manipulation() {
        var board = Board(n1: 4, n2: 4)
        board?.insertPiece(id: 1, column: 2)
        XCTAssertEqual(1, board?.grid[3][2])
       board?.removePiece(row: 3, column: 2)
        XCTAssertNil(board?.grid[3][2])
       }
    func clean(){
           var board = Board(withGrid: [[1,0,1],[0,1,0],[1,0,1]])
           if var b = board {
           b.clearGrid()
               for r in 0..<b.nbRows {
                   for c in 0..<b.nbColumns {
                       XCTAssertNil(b.grid[r][c])
                    }
                   }
                  }
                 }
    func constructor() throws {
        var n1 = -8
        var n2 = 10
        var b = Board(n1: n1, n2: n2)
        XCTAssertNil(b?.grid)
        n1 = 4
        n2 = -10
        b = Board(n1: n1, n2: n2)
        XCTAssertNil(b?.grid)
        n1 = 4
        n2 = 10
        b = Board(n1:n1, n2: n2)
        XCTAssertNotNil(b?.grid)
        XCTAssertEqual(n1, b?.nbRows)
        XCTAssertEqual(n2, b?.nbColumns)
    }
}
