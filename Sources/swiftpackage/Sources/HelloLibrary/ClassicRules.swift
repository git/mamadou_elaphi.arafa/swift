//
//  File.swift
//  
//
//  Created by etudiant on 09/02/2023.
//

import Foundation
public class ClassicRules : Rule {
    private let board: Board
    private let Minnbrcoln = 4
    private let MinnbrRow = 4
    private var isOver = false
    
    public init?(withBoard board: Board){
        guard board.nbColumns >= Minnbrcoln && board.nbRows >= MinnbrRow else {
            return nil
        }
        self.board = board
    }
    // Fonction pour déterminer si une pièce a été insérée dans une colonne donnée
    // et si elle a formé une combinaison gagnante.
   public func isValid(column: Int) -> Int {
        let grid = board.grid
        var y = 0;
        var id = 0
       // Appel de la fonction pour obtenir la dernière ligne et l'ID de la dernière pièce insérée dans cette colonne
        (y, id) = gettheLast(column: column)
       // Variables pour stocker le score actuel et la valeur de retour
        var score = 0
        
       // Définir les bornes pour la vérification des lignes, des colonnes et des diagonales
        var minX = column - 3
        if minX < 0 {
            minX = 0
        }
        var maxX = column + 3
        if maxX >= board.nbColumns {
            maxX = board.nbColumns - 1
        }
        var minY = y - 3
        if minY < 0 {
            minY = 0
        }
        var maxY = y + 3
        if maxY >= board.nbRows {
            maxY = board.nbRows - 1
        }
        
       // Vérification de la ligne horizontale
        for i in minX...maxX {
            if grid[y][i] == id {
                score += 1
            } else {
                score = 0
            }
        }
        if score >= 4 { return id }
        
       // Vérification de la colonne verticale
        for i in minY...maxY {
            if grid[i][column] == id {
                score += 1
            } else {
                score = 0
            }
        }
        if score >= 4 { return id }
        
       // Vérification de la diagonale allant du nord-ouest au sud-est
        var minCoord = min(minX, minY)
        var maxCoord = min(maxX, maxY)
        for i in minCoord...maxCoord {
            if grid[i][i] == id {
                score += 1
            } else {
                score = 0
            }
        }
        if score >= 4 { return id }
        
       // Vérification de la diagonale allant du nord-est au sud-ouest
        minCoord = min(maxX, minY)
        maxCoord = min(minX, maxY)
        for i in minCoord...maxCoord {
            if grid[i][i] == id {
                score += 1
            } else {
                score = 0
            }
        }
        if score >= 4 { return id }
        return 0
    }
    // Cette fonction privée `gettheLast` trouve la dernière case remplie dans une colonne donnée.
    private func gettheLast(column: Int) -> (Int, Int) {
        let grid = board.grid
        var y = 0;
        var id = 0
        // Boucle sur toutes les lignes de la grille.
        for i in 0..<board.nbRows {
            // Si une case dans la colonne actuelle est remplie, enregistre les coordonnées (ligne, colonne) et l'identifiant de la pièce.
            if let idCase = grid[i][column] {
                y = i
                id = idCase
                break
            }
        }
        // Renvoie les coordonnées (ligne, colonne) et l'identifiant de la pièce.
        return (y, id);
    }
    // Cette fonction publique `isGameOver` vérifie si la partie est terminée.
    public func isGameOver() -> Bool {
        isOver = true
        for i in 0..<board.nbColumns {
            if board.grid[0][i] == 0 {
                isOver = false
                break
            }
        }
        return isOver
    }
    
}
