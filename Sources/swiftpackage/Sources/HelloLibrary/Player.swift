//
//  File.swift
//  
//
//  Created by etudiant on 09/02/2023.
//

// Import the Foundation library
import Foundation

// Define a protocol "Player"
public protocol Player {
    
    // Property: "nom" of type String, with both getter and setter
    var nom: String { get set }
    
    // Property: "board" of type Board, with both getter and setter
    var board: Board { get set }
    
    // Function: "playInColumn" returns an Int
    func playInColumn() -> Int
}
