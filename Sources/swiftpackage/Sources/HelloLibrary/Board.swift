//
//  File.swift
//  
//
//  Created by etudiant on 09/02/2023.
//

import Foundation
public struct Board: CustomStringConvertible {
    // La grille du tableau qui peut contenir des valeurs nues (nil) ou des entiers (1 ou 2)
    var grid: [[Int?]];
    // Le nombre de lignes dans la grille
    let nbRows: Int;
    // Le nombre de colonnes dans la grille
    let nbColumns: Int;
    // Une table de correspondance qui mappe les entiers (1 et 2) aux valeurs de chaînes "O" et "X"
    private static let descriptionMapper: [Int?:String] = [nil:" ", 1:"O", 2:"X"];
    // Initialiseur qui permet de créer une grille avec le nombre de lignes et de colonnes données
    public init?(n1: Int = 4, n2: Int = 5) {
        // Si n1 ou n2 sont inférieurs ou égaux à 0, retournez nil
        if n1 <= 0 || n2 <= 0 {
            return nil
        }
        // Définit le nombre de lignes et de colonnes
        self.nbRows = n1
        self.nbColumns = n2
        // Initialise la grille avec des valeurs nues (nil)
        self.grid = Array(repeating: Array(repeating: nil, count: n2), count: n1)
    }
    
    public init?(withGrid grid : [[Int?]]){
        // Récupère les tailles de chaque colonne dans la grille donnée
        let sizes = grid.map{ return $0.count }
        // Vérifie que toutes les tailles correspondent à la taille de la première colonne
        let result = sizes.allSatisfy {
            $0 == grid[0].count
        }
        // Si toutes les tailles ne correspondent pas, retournez nil
        guard result else {
            return nil
        }
        // Définit le nombre de lignes et de colonnes en fonction de la grille donnée
        nbRows = grid.count
        nbColumns = grid[0].count
        // Utilise la grille donnée pour initialiser la grille
        self.grid=grid
    }
    
    
    public var description: String {
        // déclaration de la variable de sortie string qui sera retournée
        var string = String()
        // déclaration de la variable de sortie string qui sera retournée
        for row in grid {
            // ajout d'un séparateur de ligne pour séparer les lignes
            string.append("| ")
            // boucle pour parcourir les cellules dans la ligne actuelle
            for cell in row {
                // ajout de la description de la cellule dans la chaîne string. Si la cellule est vide, un espace vide est ajouté
                string.append("\(String(describing: Board.descriptionMapper[cell] ?? " "))")
                // ajout d'un séparateur de colonne pour séparer les cellules
                string.append(" | ")
            }
            // ajout d'un saut de ligne pour passer à la ligne suivante
            string.append("\n")
        }
        // ajout d'un séparateur de ligne pour séparer les lignes de la grille
        string.append("|")
        for _ in 0...grid.count{
            string.append("---|")
        }
        // ajout de l'index de colonne en dessous de la grille
        string.append("\n")
        
        string.append("|")
        for i in 0...grid.count{
            string.append(" \(i) |")
        }
        // retourne la chaîne string construite
        return string
    }
    
    private func isFull() -> Bool {
        // boucle pour parcourir les colonnes
        for column in 0...nbColumns{
            // si la colonne n'est pas pleine, retourne false
            if !isColumnFull(column: column) {
                return false
            }
        }
        // si toutes les colonnes sont pleines, retourne true
        return true
    }
    
    private func isColumnFull(column: Int) -> Bool{
        for row in 0..<nbRows {
            // si la cellule est vide, retourne false
            if grid[row][column] == nil {
                return false
            }
        }
        // si toutes les cellules sont pleines, retourne true
        return true
    }
    
    private mutating func insertPiece(id:Int, row:Int, column:Int) -> BoardResult {
        // vérifie si la ligne et la colonne sont dans les limites de la grille
        guard row >= 0 && row < nbRows && column >= 0 && column < nbColumns else {
            // retourne une erreur si les coordonnées sont en dehors de la grille
            return .failed(.outOfBound)
        }
        // vérifie si la cellule est déjà remplie
        guard grid[row][column] == nil else {
            return .failed(.unknown)
        }
        
        grid[row][column] = id
        return .ok
    }
    // Fonction pour insérer une pièce dans une colonne particulière
    public mutating func insertPiece(id:Int, column:Int) -> BoardResult {
        // Initialiser le résultat comme étant inconnu
        var result = BoardResult.unknown
        // Boucle à travers les lignes dans l'ordre inverse
        for row in (0..<nbRows ).reversed() {
            // Si la ligne est inférieure à 0, retourner une erreur "colonne pleine"
            if row < 0 {
                return .failed(.columnFull)
            }        // Appeler la fonction `insertPiece` avec les arguments `id`, `row`, et `column`
            result = insertPiece(id: id, row: row, column: column)
            // Vérifier le résultat de l'insertion
            switch result {
                // Si l'insertion a réussi, retourner `.ok`
            case .ok:
                return .ok
                // Sinon, continuer la boucle
            default:
                break
            }
        }
        return .failed(.unknown)
    }
    
    public mutating func removePiece(row:Int, column:Int) -> BoardResult {
        guard row >= 0 && row < nbRows && column >= 0 && column < nbColumns else {
            return .failed(.outOfBound)
        }
        
        grid[row][column] = nil
        return .ok
    }
    
    public mutating func clearGrid(){
        for r in 0..<nbRows {
            for c in 0..<nbColumns {
                grid[r][c] = nil
            }
        }
    }
}
