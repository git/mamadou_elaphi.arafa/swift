//
//  File.swift
//  
//
//  Created by etudiant on 09/02/2023.
//

import Foundation
public class Lecteur {
    public init() {
        
    }
    // Lit une entrée entière de l'utilisateur et la retourne.
    public func lireInt() -> Int{
        // Tente de lire une ligne de l'entrée de l'utilisateur.
        if let value = readLine() {
            // Convertit la chaîne en entier et la retourne. Si la conversion échoue, retourne -1.
            return Int(value) ?? -1
        } else {
            // Si la lecture de l'entrée échoue, retourne -1.
            return -1
        }
    }
    // Lit une entrée de ligne de l'utilisateur et la retourne sous forme de chaîne.
    public func lireLigne() -> String? {
        // Lit une ligne de l'entrée de l'utilisateur et la retourne sous forme de chaîne.
        return readLine()
    }
}
