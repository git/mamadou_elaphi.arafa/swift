//
//  File.swift
//  
//
//  Created by etudiant on 09/02/2023.
//

import Foundation

public struct Game {
    // La variable privée `board` représente le plateau de jeu
    private var board: Board
    // La variable privée `players` représente les joueurs
    private var players: [Player]
    private var numero: Int = 0
    // La variable privée `afficheur` représente l'afficheur de messages
    private var afficheur: Afficheur
    private var rule: Rule
    // Initialise le jeu avec un plateau donné, des joueurs donnés, des règles données et un afficheur donné
    public init?(withBoard board: Board, playedBy players: [Player], withRules rule: Rule, writeOn afficheur: Afficheur) {
        // Affecte les valeurs aux variables d'instance
        self.board = board
        self.players = players
        self.afficheur = afficheur
        self.rule = rule
    }
    // Initialise le jeu avec une grille donnée, des joueurs donnés, des règles données et un afficheur donné
    public init?(withGrid grid: [[Int?]], playedBy players: [Player], withRules rule: Rule, writeOn afficheur: Afficheur) {
        // Initialise un nouveau plateau avec la grille donnée
        let b = Board(withGrid: grid)
        // Vérifie que le plateau a bien été initialisé
        guard b != nil else {
            // Si ce n'est pas le cas, retourne nil
            return nil
        }
        
        board = b!
        
        self.players = players
        self.afficheur = afficheur
        self.rule = rule
    }
    // Initialise le jeu avec un nombre donné de lignes et de colonnes, des joueurs donnés, des règles données et un afficheur donné
    public init?(withNbRow nbRow: Int = 6, withNbCol nbCol: Int = 7, playedBy players: [Player], withRules rule: Rule, writeOn afficheur: Afficheur) {
        // On crée un plateau de jeu avec les dimensions spécifiées.
        let b = Board(n1: nbRow, n2: nbCol)
        // Si le plateau ne peut pas être créé, on renvoie nil.
        guard b != nil else {
            return nil
        }
        // Sinon, on définit le plateau de jeu pour l'instance actuelle de la classe Game.
        board = b!
        // On enregistre les joueurs qui joueront la partie.
        self.players = players
        // On enregistre l'afficheur qui sera utilisé pour afficher les informations sur le plateau.
        self.afficheur = afficheur
        // On enregistre les règles du jeu qui seront utilisées pour valider les mouvements des joueurs.
        self.rule = rule;
    }
    // Méthode qui permet à un joueur de jouer un tour.
    public mutating func tour() -> Player?{
        // On récupère le joueur actuel.
        let player = players[numero]
        // On initialise le résultat de l'insertion de la pièce sur le plateau.
        var result = BoardResult.unknown
        var choice = 0
        // Tant que la pièce ne peut pas être insérée sur le plateau, on demande au joueur où il souhaite insérer la pièce.
        while( result != BoardResult.ok){
            afficheur.afficherLigne(message: "\nJoueur \(player.nom), ou veut tu inserer le jeton:) ?")
            choice = player.playInColumn()
            afficheur.afficherLigne(message: "Le nombre choisi est \(choice).")
            result = board.insertPiece(id: numero + 1, column: choice)
        }
        // On vérifie si un joueur a gagné la partie.
        let winnerId = rule.isValid(column: choice)
        // Si un joueur a gagné, on affiche un message pour annoncer le gagnant et on retourne le joueur gagnant.
        if winnerId != 0 {
            afficheur.afficherLigne(message: "\(players[winnerId].nom) a gagné !")
            return players[winnerId]
        }
        joueurSuivant();
        afficheur.afficherLigne(message: board.description)
        return nil
    }
    
    public mutating func joueurSuivant() {
        numero = (numero + 1) % players.count
    }
}
