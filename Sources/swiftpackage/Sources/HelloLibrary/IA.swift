//
//  File.swift
//  
//
//  Created by etudiant on 09/02/2023.
//

import Foundation
// Définition de la classe "IA" qui conforme au protocole "Player"
public class IA: Player {
    // Propriété : "nom" de type String, avec un getter et un setter
    public var nom: String
    // Propriété : "board" de type Board, avec un getter et un setter
    public var board: Board
    // Initialisation avec deux paramètres "nom" et "board"
    public init(nom nom: String, playedOn board: Board){
        self.nom = nom
        self.board = board
    }
    // Fonction : "playInColumn" qui retourne un entier aléatoire compris entre 0 et "nbColumns"
    public func playInColumn() -> Int {
        return Int.random(in: 0..<(board.nbColumns))
    }
    
    
}
