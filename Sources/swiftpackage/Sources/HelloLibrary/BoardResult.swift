//
//  File.swift
//  
//
//  Created by etudiant on 09/02/2023.
//

import Foundation
public enum BoardResult : Equatable{
    case unknown
    case ok
    case failed(FailedReason)
}

public enum FailedReason {
    case unknown
    case columnFull
    case boardFull
    case outOfBound
}
