//
//  File.swift
//  
//
//  Created by etudiant on 09/02/2023.
//


import Foundation

// Pour avoir une collection de règles,la création d'une règle qui étend ce protocol, et qui contiendra une liste de rules permet de savoir les priorités des règles.
public protocol Rule {
    func isValid(column: Int) -> Int
    func isGameOver() -> Bool
}
