//
//  File.swift
//  
//
//  Created by etudiant on 09/02/2023.
//


import Foundation

// Définition de la classe "Human" qui conforme au protocole "Player"
public class Human : Player {
    // Propriété : "nom" de type String, avec un getter et un setter
    public var nom: String
    // Propriété : "board" de type Board, avec un getter et un setter
    public var board: Board
    private var lecteur: Lecteur
    
    // Initialisation avec trois paramètres "nom", "board" et "lecteur"
    public init(nom nom: String, playedOn board: Board, readOn lecteur: Lecteur){
        self.nom = nom
        self.board = board
        self.lecteur = lecteur
    }
    
    public func playInColumn() -> Int {
        lecteur.lireInt()
    }
    
    
}
