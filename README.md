Mon Application swift développée en Swift permet de 
* Jouer avec un ami
* Jouer contre un ordinateur
* Faire jouer deux ordinateurs l'un contre l'autre.

Pour l'Exécuter il faut
* Télécharger xcode 
* Ouvrir le workspace et executer le main
* Cliquer sur le bouton d'exécution en haut à gauche .

Dans un jeux il peut y avoir plusieurs joueurs  qui peuvent être des humains ou de iA